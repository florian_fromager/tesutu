# Tesutu

A tiny web page to exercise Hiragana and Katakana characters.

## To do

- Customizable word list
- Definition of words
- Customizable rules (number of words, etc.)