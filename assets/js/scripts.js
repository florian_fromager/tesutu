const getArr = () => {

    const arr = [
      "ao",
      "no",
      "kazoku",
      "sutoka",
      "ichi",
      "sensee",
      "kudasai",
      "tai",
      "doitsu",
      "kankokugo",
      "desu",
      "udon",
      "sushi",
      "neko",
      "inu",
      "nato",
      "niconico",
      "tanuki",
      "kitsune",
      "aniki",
      "oneesan",
      "tookyoo",
      "ginko",
      "dezato",
      "kisu",
      "kaki",
      "soseji",
      "aisu",
      "asoko",
      "soko",
      "ashita",
      "shizuoka",
      "tokei",
      "eki",
      "tochigi",
      "kasa",
      "asa",
      "hito",
      "chichi",
      "haha",
      "futago",
      "uchi",
      "naniga sukidesuka",
      "kohi",
    ];
    
    return arr;
  
  }
  
  var randomProperty = () => {
  
      let oldObj = getArr();
    let newObj = [];
  
    for ( let i = 0; i < oldObj.length; i++ ) {
  
      const keys = Object.keys( oldObj );
      const randomStr = oldObj[ keys[ keys.length * Math.random() << 0 ] ];
      
      newObj.push( randomStr );
        
      const indexOfObject = oldObj.findIndex( object => {
        return object === randomStr;
      });
  
      oldObj.splice( indexOfObject, 1 );
       
    }
  
    return newObj;
  
  };
  
  const tesutu = ( tag ) => {
  
      let str = "";
  
      const randomArr = randomProperty();
  
    for ( let i = 0; i < 10; i++ ) {
  
      str += randomArr[ i ] + "<br/>";
  
    }
    
      tag.innerHTML = str;
  
  }
  
  window.onload = function() {
  
    tesutu( hiragana );
    tesutu( katakana );
  
  };
  
  button.addEventListener("click", function() {
  
    tesutu( hiragana );
    tesutu( katakana );
  
  }); 